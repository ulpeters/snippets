We have a bunch of tiff files on a Windows Share and want to know which one are compressed.


```
# Get filelist (Windows command prompt)
dir *.tif? /s /b > filelist.txt

# Split file list in packages á 500 files (bash)
split -d -l 500 filelist.txt filelist-

# Get file info in ini format using IrfanView
FOR %G in (filelist-*.txt) do "C:\Program Files (x86)\IView\i_view32.exe" /filelist=%G /info=info-%G

# Concatenate result files (bash)
cat info-*.txt >> info.txt

# Create csv from ini (python)
python3 ini2csv.py -i $file -o $file".csv"

# Or convert individual files
for file in info-*.txt
  do python3 ini2csv.py -i $file -o $file".csv"
done
```