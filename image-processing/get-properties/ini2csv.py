import configparser, csv, re, argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input',   help='input file', required=True)
parser.add_argument('-o', '--output', help='output file', required=True)
args = parser.parse_args()

tiffs = configparser.ConfigParser()
tiffs.read(args.input, 'utf-16')

with open(args.output, 'w') as csvfile:
  csvwriter = csv.writer(csvfile, dialect='excel', delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
  csvwriter.writerow(['File name','Directory','Compression','Image dimensions','Color depth','Number of unique colors','Current memory size','File date/time'])

  for img in tiffs.sections():
    csvwriter.writerow([
      tiffs[img]['File name'],
      tiffs[img]['Directory'],
      tiffs[img]['Compression'],
      re.search('.*(?=  Pixels)', tiffs[img]['Image dimensions']).group(0),
      re.search('.*(?=   \()'   , tiffs[img]['Color depth']).group(0),
      tiffs[img]['Number of unique colors'],
      re.search('.*(?= \()'     , tiffs[img]['Current memory size']).group(0),
          re.sub(' \/','',tiffs[img]['File date/time'])
      ])