## Hiring

### Start with a job description

### Prepare an Inteview Plan

Start from the top 5-8 requirements from the job description

| Competency | Behavioiral Question | Interviewer | Rating (1-10) | Weight |
| ---------- | -------------------- | ----------- | ------------- | ------ |


### Question Types
- What if, ...
- Problem solving, e.g. prepare a flowchart from an procedure
- Creative thinink question, e.g. what trends do you see coming up...
- Brain buster, e.g. how many golf balls fit into a Airbus A380
- Behavioiral Question

### Prepepare behavioiral question
- Develop questions from top requirements
- Use open-ended questions
- Ask based on past experience
- START (e.g. about team work)
  - Situation you were in (Tell me about a project where you worked with a team)
  - Task you had
  - Action you took
  - Result of actions
