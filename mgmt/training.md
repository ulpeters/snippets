Presentation from [Tina Seufert](https://www.uni-ulm.de/en/in/psy-llf/team/tina-seufert/) Lehre 4.0, Univerität Ulm, 15.11.2017


- Define upfront
  - clear target
  - scope
  - deadline
  - competition
  - result-focuses (clear result picture)

- Tips
  - The brain processes text and pictures seperately,
    so both channels can be utilized in parallel

  - Speach and text cannot be processed in parallel,
    so avoid text on your slides

  - The brain works with references,
    so try to refer to existing knowledge
    and repeat what is already known

  - Humens lose attention after about 20 minutes,
    so change context after 20 minutes, e.g. with a task or poll

  - Thinking aloud, learning together is more effective

- Allow self-reflection in the end