# Basic Job Description Template


* __Position:__ Global Entertainment Lead
* __Reports to:__ Sr. Director Show Business
* __Department:__ Global IT 

 
| Tasks — why was this position created?          | Requirements — which skills are essential to fill this position? |
| ----------------------------------------------- | ---------------------------------------------------------------- |
| - Ensure ... plans                              | - Skilled in preparing ... such as...                            |
| - Ensure ... records ... ready for...           | - Very good ..language.. skills, written and verbal              |
| - Manage ... requests                           | - Sound experience in ... design                                 |
| - Manage, oversee and report upon...            | - Tools & Methods                                                |
| - Act a primary contact for ...                 |   - Data analyse in Microsoft Excel                              |
| - Consult ...                                   |   - Microsoft Office                                             |
| - Manage ... projects .. program                |   - SAS JMP                                                      |
| - Anything else management considers reasonable |   - Microsoft SharePoint                                         | 