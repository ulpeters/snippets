Hi `Name`,

Let’s start with one-on-one (o³) meetings to maintain good open communication and continue to build our relationship. It will be a private conversation 30 minutes once a week.

Agenda:
* The first 10 minutes are for YOUR agenda - anything you want to tell me, about anything. Your work, your family, your pets, your hobby, your challenges, your career, our working together. The primary focus of this meeting is going to be YOU.

* The middle ten minutes are for me, to share whatever I need to with you. We'll probably talk about projects you and I are working on, stuff I need from you, and things I've heard from other peers. It will NOT be a team meeting with only one attendee.

* The last 10 minutes are for us to talk about the future. In case you'll take 15 minutes, and I will too, we might not get to the last segment. But that's okay, if we've covered what YOU want to cover, and I get a few minutes.


Talk to you! `Name`