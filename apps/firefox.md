# Useful Firefox Extensions

- [Tree Style Tab by Piro (piro_or)](https://addons.mozilla.org/en-US/firefox/addon/tree-style-tab/) organizes tabs in a hierarchical tree

- [Firefox Multi-Account Containers by Mozilla Firefox](https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/) separate websites from each other to allow multiple logins and limit tracking

- [DeepL Translate: Reading & writing translator by DeepL](https://addons.mozilla.org/en-US/firefox/addon/deepl-translate/) an ai powered translator

- [Grammar and Spell Checker - LanguageTool by LanguageTooler GmbH](https://addons.mozilla.org/en-US/firefox/addon/languagetool/) an ai powered spell and grammer  checker 

- [KeePassXC-Browser by KeePassXC Team](https://addons.mozilla.org/en-US/firefox/addon/keepassxc-browser/) auto-fills passwords from KeePassXC

- [Video DownloadHelper von mig, Paul Rouget](https://addons.mozilla.org/de/firefox/addon/video-downloadhelper/) downloads videos to watch them later on offline

- [DownThemAll! von Nils Maier](https://addons.mozilla.org/de/firefox/addon/downthemall/) mass downloads files from a website, e.g. bank statements