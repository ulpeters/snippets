#!/bin/bash
docker run -it --rm --volume `pwd`/data:/data \
           --hostname vdirsyncer --name vdirsyncer \
	   --workdir /data python /data/run.sh
