## Install Python
  - Download: https://www.python.org/downloads/windows/
    - Install for all users and add to PATH
  - Download: https://support.microsoft.com/en-us/help/2977003/the-latest-supported-visual-c-downloads
    - Install

## Install Git
  - https://git-scm.com/download/win

## Install vdirsyncer
  1. Run `cmd` as administrator
  2. `pip install vdirsyncer`

## Prepare programm folder
`mkdir %userprofile%\bin\contactsync`
  - This folder should be ONLY ACCESSABLE by the user as passwords are stored in plaintext
  - All scripts and configurations are located here
  - ./contacts with vcards will be synced here
  - ./book.csv will be created here
  - The macro will execute run.bat from here and pulls book.csv from here

