Sub load()
progpath = Environ("USERPROFILE") & "\bin\contactsync"

' Shell Runs async
'ChDir progpath
'Shell progpath & "\test.bat", vbNormalFocus

' wsh runs sync
Dim wsh As Object
Set wsh = VBA.CreateObject("WScript.Shell")
Dim waitOnReturn As Boolean: waitOnReturn = True
Dim windowStyle As Integer: windowStyle = 1
wsh.Run progpath & "\test.bat", windowStyle, waitOnReturn


Range("book").ListObject.QueryTable.Refresh BackgroundQuery:=False
End Sub