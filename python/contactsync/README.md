# Calendarsync
Purpose of this project is to replicate contacts from CardDAV to Microsoft Excel.

### Components:
- Docker Container to test vdirsync (start-docker-container.sh)
- Windows shell script (vdirsync.bat) to run vdirsync
- Python script to parse carddav files to csv (parse-vcf.py)
- Script to start the vdirsync and parse script (run.bat)
- PowerQuery to load CSV to Excel (powerquery.txt)
- VBA script to update Excel with the CSV file (excel.vba)

### Pipeline:
- User clicks button in Excel which runs the load vba function
- vba starts run.bat and consequently
  - vdirsync pulls latest vcard files (*.vcf) to ./contacts/
  - parse-vcf.py parses the vcards to book.csv
- vba pulls the data via powerquery from `%userprofile%\bin\contactsync\book.csv` 


## Configuration
For Linux, set your address book url, username and password here:
  - data/carddav.url
  - data/password
  - data/username

Set your book url, username and password here:
  - config-windows.txt

## Testing
- For testing under Linux, set your address book url, username and password here:
  - carddav.url
  - password
  - username
- `run.bat` equvivalent is `run.sh`
- `vdirsync.bat` equvivalent is `vdirsync.sh`
