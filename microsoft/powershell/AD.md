Find AD group by name
```
Get-ADGroup -Filter {name -like "*sql*"} -Properties Description,info | Select Name,samaccountname,Description,info | Sort Name
```