$imageFile = <full path>
Add-Type -AssemblyName System.Drawing
$image = New-Object System.Drawing.Bitmap $imageFile
$imageWidth = $image.Width
$imageHeight = $image.Height
$imageColors = $image.PixelFormat
$imageFormat = $image.RawFormat
# https://docs.microsoft.com/en-us/previous-versions/windows/desktop/wiaaut/-wiaaut-consts-formatid