$action = New-ScheduledTaskAction -Execute 'Powershell.exe' `

  -Argument '-Command "Get-ChildItem -Path F:\Backup -File -Recurse -Force | Where-Object {($_.LastWriteTime -lt (Get-Date).AddDays(-7))}| Remove-Item -Force"'

$trigger =  New-ScheduledTaskTrigger -Daily -At 9am

Register-ScheduledTask -Action $action -Trigger $trigger -TaskName "Purge old backup" -Description "Purge files older than 1 week"