<#
.SYNOPSIS
This script takes a list of email addresses from the clipboard, retrieves their corresponding SAM account names from Active Directory, 
and copies the results as a table to the clipboard.

.ENVIRONMENT
- The Active Directory module must be installed and imported:
  1. Open PowerShell as Administrator.
  2. Run the following command to install the RSAT (Remote Server Administration Tools):
     Get-WindowsCapability -Name RSAT.ActiveDirectory* -Online | Add-WindowsCapability -Online
  3. Import the Active Directory module into your PowerShell session with:
     Import-Module ActiveDirectory
#>

# Get the list of email addresses from the clipboard
$emailAddresses = (Get-Clipboard -Raw).Split("`n")

# Initialize an array to hold the results
$results = @()

# Loop through each email address
foreach ($email in $emailAddresses) {
    # Get the user object from Active Directory
    $user = Get-ADUser -Filter {Mail -eq $email} -Properties SamAccountName

    # Check if the user was found
    if ($user) {
        # Add the email and SAM account name to the results array
        $results += [PSCustomObject]@{
            Email = $email
            SamAccountName = $user.SamAccountName
        }
    } else {
        # If the user was not found, add a placeholder
        $results += [PSCustomObject]@{
            Email = $email
            SamAccountName = "Not Found"
        }
    }
}

# Convert the results to a table and copy to clipboard
$results | Format-Table | Out-String | Set-Clipboard

Write-Host "Script completed. The results have been copied to the clipboard."
$results | Format-Table | Out-String