## Custom Theme Colors in SP Modern UI
### Create a color theme
Create from https://aka.ms/spthemebuilder and copy the string for PowerShell 


### Install SharePoint Mgmt. Shell / Connect
https://docs.microsoft.com/en-us/powershell/sharepoint/sharepoint-online/connect-sharepoint-online?view=sharepoint-ps
```
# Check if Mgmt. shell is already installed
Get-Module -Name Microsoft.Online.SharePoint.PowerShell -ListAvailable | Select Name,Version
# Install if neccessary
Install-Module -Name Microsoft.Online.SharePoint.PowerShell
# Connect
Connect-SPOService -Url https://EnterYouTenantHere-admin.sharepoint.com
```
### Install Theme
https://docs.microsoft.com/en-us/powershell/module/sharepoint-online/Add-SPOTheme?view=sharepoint-ps
```
$themepalette = @{
"themePrimary" = "#c5024f";
"themeLighterAlt" = "#fdf3f7";
"themeLighter" = "#f6cfde";
"themeLight" = "#eda7c3";
"themeTertiary" = "#dc598d";
"themeSecondary" = "#cb1a61";
"themeDarkAlt" = "#b10248";
"themeDark" = "#95013d";
"themeDarker" = "#6e012d";
"neutralLighterAlt" = "#f8f8f8";
"neutralLighter" = "#f4f4f4";
"neutralLight" = "#eaeaea";
"neutralQuaternaryAlt" = "#dadada";
"neutralQuaternary" = "#d0d0d0";
"neutralTertiaryAlt" = "#c8c8c8";
"neutralTertiary" = "#c2c2c2";
"neutralSecondary" = "#858585";
"neutralPrimaryAlt" = "#4b4b4b";
"neutralPrimary" = "#333333";
"neutralDark" = "#272727";
"black" = "#1d1d1d";
"white" = "#ffffff";
"primaryBackground" = "#ffffff";
"primaryText" = "#333333";
"bodyBackground" = "#ffffff";
"bodyText" = "#333333";
"disabledBackground" = "#f4f4f4";
"disabledText" = "#c8c8c8";
}

Add-SPOTheme -Identity "Custom Beurer" -Palette $themepalette -IsInverted $false
```