# Sets auto-replacements for Microsoft Office

# Get objects
$word = New-Object -ComObject word.application
$word.visible = $false
$entries = $word.AutoCorrect.entries

Import-Csv .\auto-correction.csv | ForEach-Object {
    $entries.add($($_.term), $($_.replacement))
}

# Clean up
$word.Quit()
$word = $null
[gc]::collect()
[gc]::WaitForPendingFinalizers()