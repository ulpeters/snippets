'https://stackoverflow.com/questions/32770318/attempting-create-view-in-access-gives-syntax-error-in-create-table-statement

Public Function CreateView()

Dim strSql As String

strSql = "CREATE VIEW temp_view AS" & vbCrLf & _
"SELECT" & vbCrLf & _
"   inventory2.[Unique ID] AS unique_id ," & vbCrLf & _
"   inventory2.[Used in Site] AS mv ," & vbCrLf & _
"   ExtraLoad.[Used in Site] AS csv" & vbCrLf & _
"FROM inventory2 , ExtraLoad" & vbCrLf & _
"WHERE inventory2.[Unique ID] =  ExtraLoad.[Unique ID];"

CurrentProject.Connection.Execute strSql

End Function