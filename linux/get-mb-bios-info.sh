#!/bin/sh

## Figure out the mainboard model and bios version

for file in /sys/devices/virtual/dmi/id/*; do
  if [ -f "$file" ]; then
    echo "Filename: $file"
    cat "$file"
    echo ""
  fi
done | less