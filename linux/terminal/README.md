

### vim config
see .vimrc


### tmux plugins to persist sessions
Download
```
mkdir -p ~/.tmux-plugins
git clone https://github.com/tmux-plugins/tmux-resurrect \
    ~/.tmux-plugins/tmux-resurrect
git clone https://github.com/tmux-plugins/tmux-continuum \
    ~/.tmux-plugins/tmux-continuum
```

Add following lines to ~/.tmux.com
```    
run-shell ~/.tmux-plugins/tmux-continuum/continuum.tmux
run-shell ~/.tmux-plugins/tmux-resurrect/resurrect.tmux
```

Reload TMUX environment with: 
```
tmux source-file ~/.tmux.conf

```

### git
```
git config --global user.email '<>'
git config --global user.name 'Toastie'
git config --global --add safe.directory /opt/docker
git config --global --add safe.directory /home/admin/projects
```