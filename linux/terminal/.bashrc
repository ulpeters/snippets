# append to the history file, don't overwrite it
shopt -s histappend

# update history before executing commands
PROMPT_COMMAND="history -a;$PROMPT_COMMAND"

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize