```bash
sudo rpm-ostree install xrdp
sudo systemctl reboot
sudo systemctl enable xrdp --now
sudo firewall-cmd --permanent --add-port=3389/tcp
sudo firewall-cmd --reload
```

Settings > System > Remote Desktop > 