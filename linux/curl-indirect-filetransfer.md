Download image and pipe the file directly to an FTP server

`curl https://ftp.halifax.rwth-aachen.de/grml/grml64-full_2022.11.iso | curl -T - ftp://username:password@ftp.example.com/boot.iso`