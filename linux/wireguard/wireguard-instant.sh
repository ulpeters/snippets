#!/bin/bash

### Purpose of this script
# Setup a central WireGuard instance (vpn server) to
# - serve as exit node
# - to generate key material / configurations for clients
# - help me to memorize WireGuard basics

### Files generated:
# <hostname>.key  - private key
# <hostname>.pub  - public key
# <hostname>.psk  - pre-shared key (PSK) per client
# <hostname>.conf - configuration file for the client

### Client configuration
# - Linux
#   - Save <hostname>.conf under /etc/wireguard/wg0.conf
#   - Run wg-quick up wg0
# - Android / iOS
#   - Scan QR-Code

### References
# https://wiki.archlinux.org/title/WireGuard

### Installation
# Install wireguard and QR-Code generator
apt-get install --yes wireguard qrencode 

### Variables
hostname="vpn.example.com"
server_ip='192.168.130.1/24'
network='192.168.130.0/24'
confdir='/etc/wireguard'
interface='ens3'

### Create key material
cd $confdir
# Function to generate private and public keys
wgkeypair() { wg genkey | (umask 0077 && tee   $1.key) | wg pubkey > $1.pub; }
# Function to generate the pre-shared key (PSK) for clients
wgpsk()     { wg genpsk | (umask 0077 && cat > $1.psk) }
# Generate key-pair for the central instance
wgkeypair $hostname
# Generate key-pairs and PSKs for the clients (peers)
peers="alice bob"
for peer in $peers; do wgkeypair $peer && wgpsk $peer; done

### Configuration
# Setup wireguard network interface on the client
ip link add dev wg0 type wireguard
ip addr add $server_ip dev wg0
wg set wg0 listen-port 51871 private-key $confdir/$hostname.key 

# Function to generate
# - configure peers on the server
# - prepare config files for the peers
wgsetpeer() {
peer=$1
ip=$2
wg set wg0                                 \
   peer           `cat $confdir/$peer.pub` \
   preshared-key  $confdir/$peer.psk       \
   allowed-ips    $ip
ip link set wg0 up

cat >$confdir/$peer.conf <<EOL
# Client config for $peer
[Interface]
PrivateKey = `cat $confdir/$peer.key`
Address = $ip
#DNS = 8.8.8.8
 
[Peer]
PublicKey = `cat $confdir/host.pub`
AllowedIPs = $network
Endpoint = $hostname:51871
#PersistentKeepalive = 15
PresharedKey = `cat $confdir/$peer.psk`
EOL

qrencode  -t ANSIUTF8 < $confdir/$peer.conf
}

wgsetpeer alice 192.168.130.2
wgsetpeer bob 192.168.130.3

# https://www.cyberciti.biz/faq/how-to-set-up-wireguard-firewall-rules-in-linux/
iptables -t nat -I POSTROUTING 1 -s $network -o $interface -j MASQUERADE
iptables -I INPUT 1 -i wg0 -j ACCEPT
iptables -I FORWARD 1 -i $interface -o wg0 -j ACCEPT
iptables -I FORWARD 1 -i wg0 -o $interface -j ACCEPT
iptables -I INPUT 1 -i $interface -p udp --dport 51871 -j ACCEPT
sysctl -w net.ipv4.ip_forward=1