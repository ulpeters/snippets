Scripts and templates to setup keys, PSKs and config files for a central wireguard instance, e.g. OPNsense,  and its peers.

1. [wg-new.sh](wg-new.sh) prepares a new `./config/` directory **(!! any existing directory will be deleted)**
2. Next the files `server.cfg` and `peers.cfg` in `./config/` need to be modified as per your requirements
3. Finally run  [wg-createconfig.sh](wg-createconfig.sh) generate keys, PSKs and configs
4. On the linux peer, copy .conf to /etc/wireguard/wg0.conf, apt install wireguard and `wg-quick up wg0`

To run in docker: `docker-compose run wg-config`
