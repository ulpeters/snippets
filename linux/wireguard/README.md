### WireGuard Snippets

- [wireguard-instant.sh](wireguard-instant.sh) - script to setup an ad-hoc central WireGuard instance ("vpn-server" 
- [wg-persistent.sh](wg-persistent.sh) - script to make the instance from above reboot safe
- [create-config/](create-config/) - scripts to only create WireGuard keys/PSKs/configs, e.g. for OPNsense
- Enable WireGuard Debugging: https://www.the-digital-life.com/wireguard-enable-logging/

