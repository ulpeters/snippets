#!/bin/bash

# Purpose: Make the running config from wg-instant.sh persistent

### References
# https://wiki.archlinux.org/title/WireGuard

### Variables
server_ip='192.168.130.1/24'
network='192.168.130.0/24'
confdir='/etc/wireguard'
interface='ens3' # $(ip link | grep -o -E "ens[0-9]")


# Safe current configuration and
# - remove the endpoint IP (assumption: endpoint IPs are dynamic)
# - add server IP
wg showconf wg0 | sed \
 -e "/^Endpoint/d" \
 -e "/^\[Interface]$/a Address = $server_ip" \
 > $confdir/wg0.conf

PostUp = iptables -t nat -I POSTROUTING 1 -s $network -o $interface -j MASQUERADE; iptables -I INPUT 1 -i %i -j ACCEPT; iptables -I FORWARD 1 -i $interface -o %i -j ACCEPT; iptables -I FORWARD 1 -i %i -o $interface -j ACCEPT; iptables -I INPUT 1 -i $interface -p udp --dport 51871 -j ACCEPT
PostDown = iptables -t nat -D POSTROUTING -s $network -o $interface -j MASQUERADE; iptables -D INPUT -i %i -j ACCEPT; iptables -D FORWARD -i $interface -o %i -j ACCEPT; iptables -D FORWARD -i %i -o $interface -j ACCEPT; iptables -D INPUT -i $interface -p udp --dport 51871 -j ACCEPT

## Rules in several lines for better readability
# wg-quick expands %i to the wireguard interface, here wg0
iptables -t nat -I POSTROUTING 1 -s $network -o $interface -j MASQUERADE; 
iptables -I INPUT 1 -i %i -j ACCEPT; 
iptables -I FORWARD 1 -i $interface -o %i -j ACCEPT; 
iptables -I FORWARD 1 -i %i -o $interface -j ACCEPT; 
iptables -I INPUT 1 -i $interface -p udp --dport 51871 -j ACCEPT

iptables -t nat -D POSTROUTING -s $network -o $interface -j MASQUERADE; 
iptables -D INPUT -i %i -j ACCEPT; 
iptables -D FORWARD -i $interface -o %i -j ACCEPT; 
iptables -D FORWARD -i %i -o $interface -j ACCEPT; 
iptables -D INPUT -i $interface -p udp --dport 51871 -j ACCEPT