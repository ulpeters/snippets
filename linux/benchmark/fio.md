https://docs.oracle.com/en-us/iaas/Content/Block/References/samplefiocommandslinux.htm

```
testfile=/path/to/testfile.fio
touch $testfile
fio --filename=$testfile --size=50GB --direct=1 --rw=randrw --bs=64k --ioengine=libaio --iodepth=64 --runtime=20 --numjobs=4 --time_based --group_reporting --name=throughput-test-job --eta-newline=1 
rm $testfile
```