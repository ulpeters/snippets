# Eigentlich lässt sich ein EXT4 Dateisystem nicht online verkleinern.
# Auf serverfault.com ist unter https://serverfault.com/a/888830/329677
# jedoch ein eleganten Weg über die initramfs beschrieben:


# resize using initramfs
# --- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
disk="/dev/sda1"
disk-size="25G"

### Prepare /etc/initramfs-tools/hooks/resizefs
touch /etc/initramfs-tools/hooks/resizefs
chmod +x /etc/initramfs-tools/hooks/resizefs
cat >/etc/initramfs-tools/hooks/resizefs<<'EOF'
#!/bin/sh

set -e

PREREQS=""

prereqs() { echo "$PREREQS"; }

case $1 in
    prereqs)
        prereqs
        exit 0
    ;;
esac

. /usr/share/initramfs-tools/hook-functions

copy_exec /sbin/e2fsck
copy_exec /sbin/resize2fs

exit 0
EOF


### Prepare /etc/initramfs-tools/scripts/local-premount/resizefs
touch /etc/initramfs-tools/scripts/local-premount/resizefs
chmod +x /etc/initramfs-tools/scripts/local-premount/resizefs
cat >/etc/initramfs-tools/scripts/local-premount/resizefs<<'EOF'
#!/bin/sh

set -e

PREREQS=""

prereqs() { echo "$PREREQS"; }

case "$1" in
    prereqs)
        prereqs
        exit 0
    ;;
esac

/sbin/e2fsck -yf $disk
/sbin/resize2fs $disk $disk-size
/sbin/e2fsck -yf $disk
EOF


### Prepare initramfs and reboot
update-initramfs -u
reboot
    
### Cleanup
rm /etc/initramfs-tools/hooks/resizefs
rm /etc/initramfs-tools/scripts/local-premount/resizefs
update-initramfs -u


# Change parition size
# --- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
tune2fs -l /dev/sda1 | grep "Block "
# Block count:	6553600
# Block size:	4096

# This equals
# 6553600 blocks * 4096 bytes / block = 26843545600 bytes
# 26843545600 / (1024^3) = 25 GB

fdisk -l /dev/sda
# Sector size 512 bytes
# Start 2048

# Calculating last sector
# 26843545600 bytes / 512 bytes/sector = 52428800 sectors
# 52428800 sectors + 2048 to start from = 52430848


fdisk /dev/sda
# - Delete partition
# - Create new starting with 2048 and end with 52430848