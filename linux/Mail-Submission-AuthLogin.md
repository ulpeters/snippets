## Mail Submission Auth Login
### BASE64 encode login and password
```
echo -ne "user@example.com" | base64
dXNlckBleGFtcGxlLmNvbQ==

echo -ne "PutYourSecretPasswordHere" | base64
UHV0WW91clNlY3JldFBhc3N3b3JkSGVyZQ==

```

### Connect
```
openssl s_client -starttls smtp -crlf -connect smtp.example.com:587
```

### Commands
```
EHLO test
AUTH LOGIN dXNlckBleGFtcGxlLmNvbQ==
UHV0WW91clNlY3JldFBhc3N3b3JkSGVyZQ==
mail from: <user@example.com>
rcpt to: <mary.doe@example.net>
data
From: John Doe <user@example.com>
To: Mary Doe <mary.doe@example.net>
Subject: Test ESMTP Auth LOGIN
Testing proves the existence of bugs not their absence
.
```

## Alternative using PowerShell
```
$creds = get-credential
Send-MailMessage –From user@example.com –To mary.doe@example.net –Subject "Test Email" –Body "Test SMTP Service from Powershell on Port 587" -SmtpServer smtp.office365.com -Credential $creds -UseSsl -Port 587

```