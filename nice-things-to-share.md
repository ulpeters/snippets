Web
  - [CVSSv3 Calculator](https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator) zur Bewertung von Sicherheitslücken
  - [composerize](https://composerize.com/) wandelt `docker run` commands in docker-compose.yml
  - [publicdomainvectors.org](https://publicdomainvectors.org) das unsplash.com für Vektorgrafiken
  - [https://github.com/danielmiessler/fabric](https://github.com/danielmiessler/fabric) LM prompts

  
Desktop (Universal)
  - Finance
    - [Portfolio Performance](https://www.portfolio-performance.info) verwaltet das eigene Depot unabhängig vom Broker. Der Clou, Kontobewegungen, Wertpapierabrechnungen, Erträge und Ausgaben lassen sich aus den jeweilige PDF Belegen dramafrei importieren. Die Open Source Software stellt transparent Erträge aus Dividenden und Kursgewinnen nach Gebühren und Steuern dar. Insgesamt präsentiert sich das in Java geschriebene Programm als sehr offen, PRs werden auf Github angenommen und die erfassten Daten können per CSV exportiert werden. Daumen hoch, ein gelungenes Projekt! 
  - Tools
    - [Ventoy](https://www.ventoy.net/) ISOs direkt vom USB Stick booten
    - [Static files http server](https://gist.github.com/willurd/5720255) eine Liste
    - [Czkawka](https://github.com/qarmin/czkawka) alternative to fslint, find duplicates, empty folders, similar images etc.
  - Youtube
    - [yt-dlp](https://github.com/yt-dlp/yt-dlp) a youtube-dl fork with additional features and fixes
    - [yt-dlg](https://github.com/yt-dlg/yt-dlg) A cross platform front-end GUI of the popular youtube-dl written in wxPython.
  - Firefox Plugins
    - https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/
  
Mac
  - [OpenCore-Legacy-Patcher](https://github.com/dortania/OpenCore-Legacy-Patcher) run e.g. macOS Monterey on a Mid-2012 MacBook Air 
