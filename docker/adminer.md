# Adminer

[Adminer](https://www.adminer.org/) is a simple web-based management tool for databases, e.g. PostgreSQL, MySQL, SQL Server or SQLite, MongoDB.


#### Prepare Adminer for debugging
```
# Start Adminer
docker run --rm --detach --name adminer adminer
# Connect to a network where e.g. a PostgrSQL server is running
docker network connect guacamole_default adminer
# Connect to a network with a Webbrowser
docker network connect terminalserver_default adminer
```

In the Terminalserver container, connect to Adminer: `firefox http://adminer:8080`

