### Dump
[pg_dump manual](https://www.postgresql.org/docs/current/app-pgdump.html)

Run within the database container `pg_dump -U <user> <database_name> > dump.sql`



### Restore
[psql manual](https://www.postgresql.org/docs/current/app-psql.html)

Create database and run `psql -U <user> <database_name> < dump.sql`
