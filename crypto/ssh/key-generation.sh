#!/bin/bash
#--Snippet to generate ssh keys------------------------------------------------
keyUser="USERNAME__PASTE_HERE"
keyFQDN="FQDN__PASTE_HERE" #full hostname
#------------------------------------------------------------------------------
keyType="ed25519"
#keyType="rsa"
keyPass=`openssl rand -base64 21` # multiple of 3 characters to avoid padding
keyPass"" # empty for no passphrase
keyDate=`date +%F` # format yyyy-mm-dd
keyName="id_"$keyType"_"$keyUser"_"$keyFQDN"_"$keyDate
ssh-keygen -t $keyType -C $keyName -N "$keyPass" -f ./$keyName
ls $keyName*
echo $keyPass > $keyName.pass
unset keyPass