#!/bin/bash

#@brief Generates TLSA Fingerprints for Let's Encrypt Intermediate Certs

# Intermediate Certificates Cross-signed by IdenTrust in pem format
# URLs: https://letsencrypt.org/certificates/
LeNew="
https://letsencrypt.org/certs/2024/r10.pem
https://letsencrypt.org/certs/2024/r11.pem
https://letsencrypt.org/certs/2024/r12.pem
https://letsencrypt.org/certs/2024/r13.pem
https://letsencrypt.org/certs/2024/r14.pem
https://letsencrypt.org/certs/2024/e5-cross.pem
https://letsencrypt.org/certs/2024/e6-cross.pem
https://letsencrypt.org/certs/2024/e7-cross.pem
https://letsencrypt.org/certs/2024/e8-cross.pem
https://letsencrypt.org/certs/2024/e9-cross.pem
"

le="https://letsencrypt.org/certs/lets-encrypt-x3-cross-signed.pem
https://letsencrypt.org/certs/lets-encrypt-r3-cross-signed.pem
https://letsencrypt.org/certs/lets-encrypt-e1.pem
https://letsencrypt.org/certs/lets-encrypt-x4-cross-signed.pem
https://letsencrypt.org/certs/lets-encrypt-r4-cross-signed.pem
https://letsencrypt.org/certs/lets-encrypt-e2.pem
"
cert=`tempfile`
echo ";TLSA Record Resource Data for Let's Encrypt Intermediate Certificates" > tlsa.txt

for url in $le
do
  curl -s $url > $cert
  echo -e -n "\n;" >> tlsa.txt
  cat $cert | openssl x509 -noout -subject >> tlsa.txt

  if [[ $url == *"encrypt-e"* ]]; then
    cat $cert | openssl x509 -noout -pubkey | openssl ec -pubin -outform DER | openssl dgst -sha256 -hex | awk '{print "2 1 1", $NF}' >> tlsa.txt
  else
    cat $cert | openssl x509 -noout -pubkey | openssl rsa -pubin -outform DER | openssl dgst -sha256 -hex | awk '{print "2 1 1", $NF}' >> tlsa.txt
  fi
done
rm $cert
