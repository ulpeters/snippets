;TLSA Record Resource Data for Let's Encrypt Intermediate Certificates

;subject=C = US, O = Let's Encrypt, CN = Let's Encrypt Authority X3
2 1 1 60b87575447dcba2a36b7d11ac09fb24a9db406fee12d2cc90180517616e8a18

;subject=C = US, O = Let's Encrypt, CN = R3
2 1 1 8d02536c887482bc34ff54e41d2ba659bf85b341a0a20afadb5813dcfbcf286d

;subject=C = US, O = Let's Encrypt, CN = E1
2 1 1 276fe8a8c4ec7611565bf9fce6dcace9be320c1b5bea27596b2204071ed04f10

;subject=C = US, O = Let's Encrypt, CN = Let's Encrypt Authority X4
2 1 1 b111dd8a1c2091a89bd4fd60c57f0716cce50feeff8137cdbee0326e02cf362b

;subject=C = US, O = Let's Encrypt, CN = R4
2 1 1 e5545e211347241891c554a03934cde9b749664a59d26d615fe58f77990f2d03

;subject=C = US, O = Let's Encrypt, CN = E2
2 1 1 bd936e72b212ef6f773102c6b77d38f94297322efc25396bc3279422e0c89270
