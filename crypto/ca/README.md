## CSR > pkcs12 certificate


1. Generate CSR and private key using OpenSSL (resulting files: `csr.pem`, `privkey.pem`)
```
openssl req -nodes -new -newkey rsa:2048 -sha256 -out csr.pem
```

2. Submit `csr.pem` to CA, e.g. [CAcert](https://www.cacert.org/) and obtain signed certificate (resulting file: `cert.pem`)


3. Bundle cert.pem and privkey.pem into pkcs12 (resultig file: `certificate.pfx`)
```
openssl pkcs12 -export -out certificate.pfx \
-inkey privkey.pem \
-in cert.pem
```